#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

SCRIPT_PATH="$(realpath "$(dirname "$0")")"

cd "${SCRIPT_PATH}"/..

function cleanup {
  chown --recursive ${HOST_UID}:${HOST_GID} /tmp/coverage_reports/cov_html
}

trap cleanup EXIT

python -m pytest \
        --cov-report html:/tmp/coverage_reports/cov_html \
        --cov-report term \
        --cov=api \
        --cov=autoapi \
        tests/api/unit_tests/
