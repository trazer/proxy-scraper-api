#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

SCRIPT_PATH="$(realpath "$(dirname "$0")")"

cd "${SCRIPT_PATH}"

LOCAL_IMAGE=proxy-scraper-api:${CI_COMMIT_SHORT_SHA}

docker build \
    --tag ${LOCAL_IMAGE} \
    --file Dockerfile-api ..
