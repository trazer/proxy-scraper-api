#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

SCRIPT_PATH="$(realpath "$(dirname "$0")")"

cd "${SCRIPT_PATH}"/../src

python -m autoapi
