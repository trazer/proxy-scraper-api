#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

SCRIPT_PATH="$(realpath "$(dirname "$0")")"

cd "${SCRIPT_PATH}"/..

LOCAL_IMAGE=proxy-scraper-api-unit-tests:${CI_COMMIT_SHORT_SHA}

docker build \
    --tag ${LOCAL_IMAGE} \
    --file ./docker/Dockerfile-api-unit-tests .
