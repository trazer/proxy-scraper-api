#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

SCRIPT_PATH="$(realpath "$(dirname "$0")")"

cd "${SCRIPT_PATH}"

function cleanup {
  echo "Cleaning docker-compose"
  docker-compose -f docker-compose-swagger-ui.yaml rm --force
}
trap cleanup EXIT

export API_IMAGE=proxy-scraper-api:${CI_COMMIT_SHORT_SHA}

docker-compose -f docker-compose-swagger-ui.yaml up --abort-on-container-exit
