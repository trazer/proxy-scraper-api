#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

SCRIPT_PATH="$(realpath "$(dirname "$0")")"

cd "${SCRIPT_PATH}"/..

function cleanup {
  echo "Cleaning docker-compose"
  docker-compose -f ./docker/docker-compose-api-unit-tests.yaml rm --force
}
trap cleanup EXIT

mkdir -p .coverage_reports

export HOST_UID=$(id -u $(whoami))
export HOST_GID=$(id -g $(whoami))
export API_UNIT_TESTS_IMAGE=proxy-scraper-api-unit-tests:${CI_COMMIT_SHORT_SHA}

docker-compose -f ./docker/docker-compose-api-unit-tests.yaml up --abort-on-container-exit
