import json
import os

import pytest
from ruamel.yaml import YAML

from autoapi import app

HEADERS = {
    'accept': 'application/json',
    'Content-Type': 'application/json',
}

_CURRENT_PATH = os.path.split(__file__)[0]


@pytest.fixture(scope='module')
def client():
    with app.app.test_client() as c:
        yield c


def test_root_get(client):

    response = client.get(
        '/',
        headers=HEADERS,
    )

    assert response.status_code == 200

    apiVersions = json.loads(response.get_data(as_text=True))

    assert isinstance(apiVersions, list)

    assert len(apiVersions) == 1

    assert len([apiVersion for apiVersion in apiVersions if apiVersion['version'] == 'v1']) == 1


def test_root_ensure_other_methods_not_implemented(client):

    response = client.post(
        '/',
        headers=HEADERS,
    )
    assert response.status_code == 405

    response = client.put(
        '/',
        headers=HEADERS,
    )
    assert response.status_code == 405

    response = client.patch(
        '/',
        headers=HEADERS,
    )
    assert response.status_code == 405

    response = client.delete(
        '/',
        headers=HEADERS,
    )
    assert response.status_code == 405


def test_v1_get(client):

    response = client.get(
        '/v1',
        headers=HEADERS,
    )

    assert response.status_code == 200

    apiVersion = json.loads(response.get_data(as_text=True))

    assert isinstance(apiVersion, dict)

    assert apiVersion['version'] == 'v1'

    assert isinstance(apiVersion['latest'], bool)
    assert apiVersion['latest']

    assert isinstance(apiVersion['stable'], bool)
    assert not apiVersion['stable']

    assert isinstance(apiVersion['deprecated'], bool)
    assert not apiVersion['deprecated']

    # Ensure that attributes are defined in the openapi.yaml
    yaml = YAML(typ='safe')
    with open(os.path.join(_CURRENT_PATH, '../../../openapi/openapi.yaml'), 'rt') as file:
        openapiSpec = yaml.load(file)
    apiVersionSchema = openapiSpec['components']['schemas']['ApiVersion']
    for propertyName in apiVersionSchema['properties'].keys():
        assert propertyName in apiVersion
    for propertyName in apiVersion.keys():
        assert propertyName in apiVersionSchema['properties']


def test_v1_ensure_other_methods_not_implemented(client):

    response = client.post(
        '/v1',
        headers=HEADERS,
    )
    assert response.status_code == 405

    response = client.put(
        '/v1',
        headers=HEADERS,
    )
    assert response.status_code == 405

    response = client.patch(
        '/v1',
        headers=HEADERS,
    )
    assert response.status_code == 405

    response = client.delete(
        '/v1',
        headers=HEADERS,
    )
    assert response.status_code == 405
