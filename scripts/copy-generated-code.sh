#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

cd "$(dirname "$0")"

PROJECT_ROOT="$(realpath ..)"
LOCAL_GENERATED_CODE_FOLDER_PATH="$(realpath ../openapi/generated_code)"

# Copy generated code for autoapi in src folder
rm -r -f ${PROJECT_ROOT}/src/autoapi
mkdir -p ${PROJECT_ROOT}/src/autoapi
cp --recursive ${LOCAL_GENERATED_CODE_FOLDER_PATH}/python-flask-custom/src/autoapi/. \
    ${PROJECT_ROOT}/src/autoapi/

# Delete artifacts
rm -r -f ${LOCAL_GENERATED_CODE_FOLDER_PATH}
