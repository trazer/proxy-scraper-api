#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

cd "$(dirname "$0")"

REMOTE_IMAGE_NAME=registry.gitlab.com/trazer/custom-openapi-generator-wrapper/custom-openapi-generator-wrapper:latest
LOCAL_IMAGE_NAME=custom-openapi-generator-wrapper:latest

docker pull ${REMOTE_IMAGE_NAME}

docker tag ${REMOTE_IMAGE_NAME} ${LOCAL_IMAGE_NAME}
