#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

cd "$(dirname "$0")"

python3.7 -m venv --clear ~/.virtualenvs/proxy-scraper-api
source ~/.virtualenvs/proxy-scraper-api/bin/activate
python -m pip install --upgrade pip
pip install --upgrade setuptools
