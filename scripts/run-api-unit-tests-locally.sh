#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

SCRIPT_PATH="$(realpath "$(dirname "$0")")"

cd "${SCRIPT_PATH}"

export CI_COMMIT_SHORT_SHA=$(git rev-parse --short HEAD)

../docker/build-docker-api-unit-tests.sh

../docker/run-docker-compose-api-unit-tests.sh
