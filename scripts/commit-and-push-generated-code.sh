#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

cd "$(dirname "$0")"/..

if output=$(git status --porcelain) && [ -z "$output" ]; then
    echo "Working directory clean"
else
    echo "Uncommitted changes"
    git checkout ${CI_COMMIT_REF_NAME}
    git status
    git add --all ./src/autoapi
    git status
    git commit \
        --message "Automatic commit of generated api code from pipeline ${CI_JOB_ID}" \
        --message "Pipeline URL: ${CI_JOB_URL}"
    git status
    git branch
    git remote
    git fetch
    git push --force origin ${CI_COMMIT_REF_NAME}
    git status
fi
