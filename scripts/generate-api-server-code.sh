#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

cd "$(dirname "$0")"

LOCAL_OPENAPI_FOLDER_PATH="$(realpath ../openapi)"
LOCAL_GENERATED_CODE_FOLDER_PATH="$(realpath ../openapi/generated_code)"

OPENAPI_FOLDER_PATH="/local/openapi"
GENERATED_CODE_FOLDER_PATH="/local/generated_code"

OPENAPI_FILE_NAME="openapi.yaml"
GENERATOR_NAME="python-flask-custom"
GENERATOR_CONFIG_FILE_NAME="config.json"

IMAGE_NAME=custom-openapi-generator-wrapper:latest

PROJECT_ROOT="$(realpath ..)"

# Create generated_code directory
mkdir -p ${LOCAL_GENERATED_CODE_FOLDER_PATH}

# Generate code
docker run \
    --rm \
    --volume ${LOCAL_OPENAPI_FOLDER_PATH}:${OPENAPI_FOLDER_PATH} \
    --volume ${LOCAL_GENERATED_CODE_FOLDER_PATH}:${GENERATED_CODE_FOLDER_PATH} \
    --env HOST_UID=$(id -u $(whoami)) \
    --env HOST_GID=$(id -g $(whoami)) \
    --env OPENAPI_FOLDER_PATH=${OPENAPI_FOLDER_PATH} \
    --env GENERATED_CODE_FOLDER_PATH=${GENERATED_CODE_FOLDER_PATH} \
    --env OPENAPI_FILE_NAME=${OPENAPI_FILE_NAME} \
    --env GENERATOR_NAME=${GENERATOR_NAME} \
    --env GENERATOR_CONFIG_FILE_NAME=${GENERATOR_CONFIG_FILE_NAME} \
    ${IMAGE_NAME}
