#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

cd "$(dirname "$0")"

export CI_COMMIT_SHORT_SHA=$(git rev-parse --short HEAD)

../docker/build-docker-api.sh

../docker/run-docker-compose-swagger-ui.sh
