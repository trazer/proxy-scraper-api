import connexion
import six

from autoapi.models.api_version import ApiVersion  # noqa: E501
from autoapi import util
import api


def root_get():  # noqa: E501
    """List API versions

     # noqa: E501


    :rtype: List[ApiVersion]
    """
    return api.root_get()


def v1_get():  # noqa: E501
    """Get information on the API version 1

     # noqa: E501


    :rtype: ApiVersion
    """
    return api.v1_get()
