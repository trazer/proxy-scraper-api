#!/usr/bin/env python3

import os

import connexion

from flask_cors import CORS
from autoapi import encoder


def _initialize_app():

    _CURRENT_PATH = os.path.split(__file__)[0]

    app = connexion.App(
        __name__,
        specification_dir=os.path.join(_CURRENT_PATH, './openapi/')
    )

    app.app.json_encoder = encoder.JSONEncoder

    app.add_api(
        'openapi.yaml',
        arguments={'title': 'Proxy-Scraper API Gateway'},
        pythonic_params=True
    )

    # TODO: Not necessarily safe. Check before deploying.
    CORS(app.app, resources={r'/*': {'origins': '*'}})

    return app


app = _initialize_app()
