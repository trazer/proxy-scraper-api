#!/usr/bin/env python3

import connexion

from flask_cors import CORS
from autoapi import app
from autoapi import encoder


def main():
    app.run(port=8082)


if __name__ == '__main__':
    main()
