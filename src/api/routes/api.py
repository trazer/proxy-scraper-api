from autoapi.models.api_version import ApiVersion as AutoApiApiVersion


AUTO_API_API_VERSION_V1 = AutoApiApiVersion(
    deprecated=False,
    latest=True,
    stable=False,
    version='v1',
)

AUTO_API_API_VERSIONS = [
    AUTO_API_API_VERSION_V1,
]


def root_get():
    return AUTO_API_API_VERSIONS, 200


def v1_get():
    return AUTO_API_API_VERSION_V1, 200
